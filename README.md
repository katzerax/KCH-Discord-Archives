# KCH-Discord-Archives
Kitty Cat Hell (KCH)  was a discord server created on February 8, 2018 and ran by [Moonkitti](https://www.youtube.com/channel/UCfn--_mAGkhYWP2NYa2NBwg) in order to run livestream QnAs for her YouTube channel. The server was a sequel to a server called Cat Cast Calls (CCC), originally ran by [LzrdWzrd](https://www.youtube.com/c/LZRDWZRD/). After LzrdWzrd was outed as a homophobic racist, Moon took over CCC and then decided to create KCH as a replacement. 

KCH lasted a few years, but was ultimately deleted by Moon on March 20, 2020. There were several reasons for the deletion, but notably was the utilization of KCH by some members to create frequent interpersonal drama. Moon allowed the archival of some channels right before deletion. 

These are those archives. The times were in US Pacific Time when the channels were archived. Everything was archived on March 20, 2020 (3/20/2020) unless a different date is in the name of the file, for example the #welcome channel was archived on "1 15 20" (January 15, 2020).
